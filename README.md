# My scripts

Thoses scripts are licenced under GPL-3 (see LICENCE for more information).

## borg-wrapper

A wrapper that run `borg create` or `borg prune` and send an email when
finished.

## freemobile

Sent SMS to your own phone through Free Mobile.

## maxmind-db-update

Quick and dirty script to downlaod MaxMind GeoIP databases.

## netsnmp-ifalias

### Installation

Install `snmp_passpersist` (eg. `pip install snmp-passpersist`).

Put the `ifalias` script where you want on your system (`/usr/local/bin/` is a
good choice).

Add this line to your snmpd configuration file :

```text
pass_persist .1.3.6.1.2.1.31.1.1.1.18 /usr/local/bin/ifalias
```

### Code

Python is invoked with option `-u` because `snmp_passpersist` does not flush
it's output after writing to stdout.


## sshfp

Generate SSHFP records for a given hostname by running ssh-keyscan.

Inspired by [hash-slinger](https://people.redhat.com/pwouters/hash-slinger/).


## zoneinfo

Print commonly queried DNS records (SOA, NS, MX, A, AAAA, CNAME, TXT, SPF) for
a given zone.
