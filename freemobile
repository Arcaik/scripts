#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import argparse
import logging
import requests


def status(exception):
    status_code = exception.response.status_code

    if status_code == 400:
        message = "A mandatory parameter is missing"
    elif status_code == 402:
        message = "Too many SMS sent in a short period"
    elif status_code == 403:
        message = "Service unavailable for this customer"
    elif status_code == 500:
        message = "An error occured on the server side"
    else:
        message = "Unknown error: {}".format(exception)

    return message


def send_sms(token, password, message):
    if not message:
        logging.error("Can't send empty message")
        return

    url = "https://smsapi.free-mobile.fr/sendmsg"

    params = {
        "user": token,
        "pass": password,
        "msg": message,
    }

    try:
        requests.get(url, params=params).raise_for_status()
    except requests.exceptions.HTTPError as e:
        logging.error(status(e))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--debug", action="store_true",
        help="Print debug messages"
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true",
        help="Print more verbose messages"
    )
    parser.add_argument("token")
    parser.add_argument("password")
    parser.add_argument("message", nargs="?")

    args = parser.parse_args()

    if args.debug:
        level = logging.DEBUG
    elif args.verbose:
        level = logging.INFO
    else:
        level = logging.WARNING

    logging.basicConfig(level=level, format="%(levelname)s: %(message)s")

    return args


if __name__ == "__main__":
    args = parse_args()

    message = args.message if args.message else sys.stdin.read()

    send_sms(args.token, args.password, message)
