#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

"""
Prints commonly queried informations for a given zone
"""

import argparse
import logging
import dns.resolver


QTYPES = ["SOA", "NS", "MX", "A", "AAAA", "CNAME", "TXT", "SPF"]


def query(zone, qtype):
    logging.debug("Querying {} on zone {}".format(qtype, zone))

    try:
        answers = dns.resolver.query(zone, qtype)
    except (dns.resolver.NoAnswer, dns.resolver.NXDOMAIN):
        return None
    except dns.resolver.Timeout:
        logging.info("Timed out while querying {} of {}".format(qtype, zone))
        return None

    return answers


def parse_args():
    parser = argparse.ArgumentParser(
        description="Prints commonly queried informations for a given zone"
    )
    parser.add_argument(
        "-d", "--debug", action="store_true",
        help="Print debug messages"
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true",
        help="Print more verbose messages"
    )
    parser.add_argument(
        "-q", "--qtype", dest="qtypes", action="append", choices=QTYPES
    )
    parser.add_argument("zones", metavar="ZONE", nargs="+")

    args = parser.parse_args()

    if args.debug:
        level = logging.DEBUG
    elif args.verbose:
        level = logging.INFO
    else:
        level = logging.WARNING

    logging.basicConfig(level=level, format="%(levelname)s: %(message)s")

    if not args.qtypes:
        args.qtypes = QTYPES

    return args


if __name__ == "__main__":
    args = parse_args()

    for i, zone in enumerate(args.zones):
        if i > 0:
            print()

        print("=== {:^15} ===".format(zone))

        for qtype in args.qtypes:
            answers = query(zone, qtype)

            if not answers:
                print("{:<6}: no records".format(qtype))
                continue

            for rdata in answers:
                print("{:<6}: {}".format(qtype, rdata))
